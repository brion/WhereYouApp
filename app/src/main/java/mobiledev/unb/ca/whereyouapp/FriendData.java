package mobiledev.unb.ca.whereyouapp;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by rcase on 03/04/16.
 */
public class FriendData {
    public String email;
    public String key;

    FriendData(String email, String key){
        this.email = email;
        this.key = key;
    }
}
